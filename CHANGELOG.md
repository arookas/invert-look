
## 0.2.0

- Add compatibility for Imperium freecam mode.
- Add compatibility for ControlCompany ghost-mode and control-mode cameras.
- Add settings to enable/disable specific compatibility patches.
- Update TooManyEmotes compatibility.

## 0.1.1

- Add compatibility for TooManyEmotes third-person camera.

## 0.1.0

- Initial release.
