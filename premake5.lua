
workspace "InvertLook"
  configurations { "Debug", "Release" }

project "InvertLook"
  kind "SharedLib"
  language "C#"
  targetdir "bin/%{cfg.buildcfg}"

  files {
    "source/core.cs",
    "source/guid.cs",
    "source/settings.cs",
    "source/patches/lethalcompany.cs",
    "source/patches/toomanyemotes.cs",
    "source/patches/controlcompany.cs",
    "source/patches/imperium.cs",
  }

  links {
    "lib/BepInEx.dll",
    "lib/0Harmony.dll",
    "lib/Assembly-CSharp.dll",
    "lib/UnityEngine.dll",
    "lib/UnityEngine.CoreModule.dll",
    "lib/LethalConfig.dll",
  }

  postbuildcommands {
    -- generate package folder for thunderstore
    "{MKDIR} %[%{wks.location}/package]",

    -- copy output DLL file
    "{COPYFILE} %[%{wks.location}/%{cfg.buildtarget.abspath}] %[%{wks.location}/package/%{cfg.buildtarget.name}]",

    -- copy auxiliary files for package
    "{COPYFILE} %[%{wks.location}/manifest.json] %[%{wks.location}/package/manifest.json]",
    "{COPYFILE} %[%{wks.location}/CHANGELOG.md]  %[%{wks.location}/package/CHANGELOG.md]",
    "{COPYFILE} %[%{wks.location}/README.md]     %[%{wks.location}/package/README.md]",
    "{COPYFILE} %[%{wks.location}/icon.png]      %[%{wks.location}/package/icon.png]",
  }

  filter "configurations:Debug"
    symbols "On"

  filter "configurations:Release"
    optimize "On"
