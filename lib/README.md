
## Library Folder

Copy the following DLLs here in order to build:

- From _BepInEx/core_:
  - BepInEx.dll
  - 0Harmony.dll
- From _Lethal Company/Lethal Company_Data/Managed_:
  - Assembly-CSharp.dll
  - UnityEngine.dll
  - UnityEngine.CoreModule.dll
- From _LethalConfig/plugins/LethalConfig_:
  - LethalConfig.dll
