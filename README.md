
## Invert Look v0.2.0

A mod to add better invert look options.
The vanilla game has only an invert Y setting;
this mod allows you to invert on each axis individually.

| Config | Description |
|--------|-------------|
| InvertX | Inverts look on the x-axis (left/right) |
| InvertY | Inverts look on the y-axis (up/down) |

> Note: Changing the settings mid-game (e.g. LethalConfig) does not require a restart.

### Compatibility

Compatibility is added with the following mods:

| Mod | Description |
|-----|-------------|
|[ControlCompany](https://thunderstore.io/c/lethal-company/p/ControlCompany/ControlCompany/)|Ghost-mode & control-mode camera|
|[Imperium](https://thunderstore.io/c/lethal-company/p/giosuel/Imperium/)|Freecam mode|
|[TooManyEmotes](https://thunderstore.io/c/lethal-company/p/FlipMods/TooManyEmotes/)|Third-person camera when performing an emote|

Compatibility patches are loaded automatically when the target mod is found.
A specific patch may be disabled in the settings.
