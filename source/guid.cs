
namespace arookas.InvertLook {

public static class PluginGuid {

  public const string CONTROL_COMPANY = "ControlCompany.ControlCompany";
  public const string IMPERIUM        = "giosuel.Imperium";
  public const string LETHAL_CONFIG   = "ainavt.lc.lethalconfig";
  public const string TOO_MANY_EMOTES = "FlipMods.TooManyEmotes";

}

}
