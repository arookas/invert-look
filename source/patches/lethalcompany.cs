
using GameNetcodeStuff;
using HarmonyLib;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;

namespace arookas.InvertLook {

/// <summary>
/// Invert look patch for the base game.
/// </summary>
internal class LethalCompanyPatch {

  /// <summary>
  /// Field info for <c>IngamePlayerSettings.Settings.invertYAxis</c>.
  /// </summary>
  private static FieldInfo InvertYAxis = AccessTools.Field(
    typeof(IngamePlayerSettings.Settings),
    nameof(IngamePlayerSettings.Settings.invertYAxis)
  );

  [HarmonyPatch(typeof(PlayerControllerB), "PlayerLookInput")]
  [HarmonyTranspiler]
  private static IEnumerable<CodeInstruction> Patch(
    IEnumerable<CodeInstruction> code
  ) {
    var instructions = new List<CodeInstruction>(code);

    int index = instructions.FindIndex(
      i => i.LoadsField(InvertYAxis)
    );

    if (index < 0) {
      Plugin.LogSource.LogError($"Player input is not patched...");

      return instructions;
    }

    index -= 2;
    instructions.RemoveRange(index, 11);

    instructions.InsertRange(index, new List<CodeInstruction> {
      new CodeInstruction(OpCodes.Ldloc, 0),
      CodeInstruction.Call(typeof(Plugin), nameof(Plugin.InvertLook)),
      new CodeInstruction(OpCodes.Stloc, 0),
    });

    Plugin.LogSource.LogInfo($"Player input is patched!");

    return instructions;
  }

}

}
