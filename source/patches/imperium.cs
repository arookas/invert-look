
using HarmonyLib;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;

namespace arookas.InvertLook {

/// <summary>
/// Compatibility patch for Imperium mod.
/// </summary>
public class ImperiumPatch {

  /// <summary>
  /// Checks whether to apply the patch.
  /// The patch is applied if both the mod is loaded and
  /// the user config indicates that the patch is enabled.
  /// </summary>
  public static bool Enabled =>
    Plugin.IsPluginLoaded(PluginGuid.IMPERIUM) &&
    Plugin.Instance.Settings.TooManyEmotesEnabled.Value
  ;

  /// <summary>
  /// Field info for <c>IngamePlayerSettings.Settings.lookSensitivity</c>.
  /// </summary>
  private static FieldInfo LookSensitivity = AccessTools.Field(
    typeof(IngamePlayerSettings.Settings),
    nameof(IngamePlayerSettings.Settings.lookSensitivity)
  );

  [HarmonyPatch(
    "Imperium.MonoBehaviours.ImpFreecam, giosuel.Imperium",
    "Update"
  )]
  [HarmonyTranspiler]
  private static IEnumerable<CodeInstruction> Patch(
    IEnumerable<CodeInstruction> code
  ) {
    var instructions = new List<CodeInstruction>(code);

    int index = instructions.FindIndex(
      i => i.LoadsField(LookSensitivity)
    );

    if (index < 0) {
      Plugin.LogSource.LogError($"Imperium is not patched...");

      return instructions;
    }

    index -= 11;

    instructions.InsertRange(index, new List<CodeInstruction> {
      new CodeInstruction(OpCodes.Ldloc, 1),
      CodeInstruction.Call(typeof(Plugin), nameof(Plugin.InvertLook)),
      new CodeInstruction(OpCodes.Stloc, 1),
    });

    Plugin.LogSource.LogInfo($"Imperium is patched!");

    return instructions;
  }

}

}
