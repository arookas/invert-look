
using HarmonyLib;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;

namespace arookas.InvertLook {

/// <summary>
/// Compatibility patch for TooManyEmotes mod.
/// </summary>
public class TooManyEmotesPatch {

  /// <summary>
  /// Checks whether to apply the patch.
  /// The patch is applied if both the mod is loaded and
  /// the user config indicates that the patch is enabled.
  /// </summary>
  public static bool Enabled =>
    Plugin.IsPluginLoaded(PluginGuid.TOO_MANY_EMOTES) &&
    Plugin.Instance.Settings.TooManyEmotesEnabled.Value
  ;

  /// <summary>
  /// Field info for <c>IngamePlayerSettings.Settings.lookSensitivity</c>.
  /// </summary>
  private static FieldInfo LookSensitivity = AccessTools.Field(
    typeof(IngamePlayerSettings.Settings),
    nameof(IngamePlayerSettings.Settings.lookSensitivity)
  );

  [HarmonyPatch(
    "TooManyEmotes.Patches.ThirdPersonEmoteController, TooManyEmotes",
    "UseFreeCamWhileEmoting"
  )]
  [HarmonyTranspiler]
  private static IEnumerable<CodeInstruction> Patch(
    IEnumerable<CodeInstruction> code
  ) {
    var instructions = new List<CodeInstruction>(code);

    int index = instructions.FindIndex(
      i => i.LoadsField(LookSensitivity)
    );

    if (index < 0) {
      Plugin.LogSource.LogError($"TooManyEmotes is not patched...");

      return instructions;
    }

    index += 4;

    instructions.InsertRange(index, new List<CodeInstruction> {
      new CodeInstruction(OpCodes.Ldloc_S, 16),
      CodeInstruction.Call(typeof(Plugin), nameof(Plugin.InvertLook)),
      new CodeInstruction(OpCodes.Stloc_S, 16),
    });

    Plugin.LogSource.LogInfo($"TooManyEmotes is patched!");

    return instructions;
  }

}

}
