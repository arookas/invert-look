
using HarmonyLib;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;

namespace arookas.InvertLook {

/// <summary>
/// Compatibility patch for ControlCompany mod.
/// </summary>
public class ControlCompanyPatch {

  /// <summary>
  /// Checks whether to apply the patch.
  /// The patch is applied if both the mod is loaded and
  /// the user config indicates that the patch is enabled.
  /// </summary>
  public static bool Enabled =>
    Plugin.IsPluginLoaded(PluginGuid.CONTROL_COMPANY) &&
    Plugin.Instance.Settings.ControlCompanyEnabled.Value
  ;

  /// <summary>
  /// Field info for <c>IngamePlayerSettings.Settings.invertYAxis</c>.
  /// </summary>
  private static FieldInfo InvertYAxis = AccessTools.Field(
    typeof(IngamePlayerSettings.Settings),
    nameof(IngamePlayerSettings.Settings.invertYAxis)
  );

  [HarmonyPatch(
    "ControlCompany.Core.CustomPlayerController, ControlCompany",
    "Rotate"
  )]
  [HarmonyTranspiler]
  private static IEnumerable<CodeInstruction> Patch(
    IEnumerable<CodeInstruction> code
  ) {
    var instructions = new List<CodeInstruction>(code);

    int index = instructions.FindIndex(
      i => i.LoadsField(InvertYAxis)
    );

    if (index < 0) {
      Plugin.LogSource.LogError($"ControlCompany is not patched...");

      return instructions;
    }

    index -= 2;
    instructions.RemoveRange(index, 15);

    instructions.InsertRange(index, new List<CodeInstruction> {
      new CodeInstruction(OpCodes.Ldloc, 0),
      CodeInstruction.Call(typeof(Plugin), nameof(Plugin.InvertLook)),
      new CodeInstruction(OpCodes.Stloc, 0),
    });

    Plugin.LogSource.LogInfo($"ControlCompany is patched!");

    return instructions;
  }

}

}
