
using BepInEx.Configuration;
using LethalConfig;
using LethalConfig.ConfigItems;
using LethalConfig.ConfigItems.Options;

namespace arookas.InvertLook {

/// <summary>
/// Plugin settings class.
/// </summary>
public class PluginSettings {

  /// <summary>
  /// Access BepInEx configuration file.
  /// </summary>
  public ConfigFile ConfigFile { get; private set; }

  /// <summary>
  /// Access invert x-axis setting.
  /// </summary>
  public ConfigEntry<bool> InvertX { get; private set; }

  /// <summary>
  /// Access invert y-axis setting.
  /// </summary>
  public ConfigEntry<bool> InvertY { get; private set; }

  /// <summary>
  /// Access ControlCompany compatibility enabled setting.
  /// </summary>
  public ConfigEntry<bool> ControlCompanyEnabled { get; private set; }

  /// <summary>
  /// Access Imperium compatibility enabled setting.
  /// </summary>
  public ConfigEntry<bool> ImperiumEnabled { get; private set; }

  /// <summary>
  /// Access TooManyEmotes compatibility enabled setting.
  /// </summary>
  public ConfigEntry<bool> TooManyEmotesEnabled { get; private set; }

  /// <summary>
  /// Load plugin settings from the given configuration.
  /// </summary>
  /// <param name="config">
  /// Configuration file to load, via <c>BaseUnityPlugin.Config</c>.
  /// </param>
  public PluginSettings(ConfigFile config) {
    ConfigFile = config;

    InvertX = config.Bind(
      "Input", "InvertX", false,
      "Invert look on the X-axis (left/right)."
    );

    InvertY = config.Bind(
      "Input", "InvertY", false,
      "Invert look on the Y-axis (up/down)."
    );

    ControlCompanyEnabled = config.Bind(
      "Compatibility", "ControlCompanyEnabled", true,
      "Patch ghost-mode and control-mode cameras."
    );

    ImperiumEnabled = config.Bind(
      "Compatibility", "ImperiumEnabled", true,
      "Patch free camera."
    );

    TooManyEmotesEnabled = config.Bind(
      "Compatibility", "TooManyEmotesEnabled", true,
      "Patch third-person emote camera."
    );

    if (Plugin.IsPluginLoaded(PluginGuid.LETHAL_CONFIG)) {
      AddLethalConfigItems();
    }
  }

  /// <summary>
  /// Add LethalConfig items for each config entry.
  /// This is a separate method in the event LethalConfig.dll is not present.
  /// </summary>
  private void AddLethalConfigItems() {
    AddLethalConfigItem(
      InvertX,
      "Invert X-Axis",
      restart: false
    );

    AddLethalConfigItem(
      InvertY,
      "Invert Y-Axis",
      restart: false
    );

    AddLethalConfigItem(
      ControlCompanyEnabled,
      "ControlCompany",
      restart: true
    );

    AddLethalConfigItem(
      ImperiumEnabled,
      "Imperium",
      restart: true
    );

    AddLethalConfigItem(
      TooManyEmotesEnabled,
      "TooManyEmotes",
      restart: true
    );
  }

  /// <summary>
  /// Creates a LethalConfig boolean checkbox item.
  /// </summary>
  /// <param name="entry">
  /// Config entry to bind.
  /// </param>
  /// <param name="name">
  /// Display name in the UI.
  /// </param>
  /// <param name="restart">
  /// Whether the setting requires a restart.
  /// </param>
  private void AddLethalConfigItem(
    ConfigEntry<bool> entry, string name, bool restart
  ) {
    LethalConfigManager.AddConfigItem(
      new BoolCheckBoxConfigItem(
        entry, new BoolCheckBoxOptions {
          Name = name,
          RequiresRestart = restart,
        }
      )
    );
  }

}

}
