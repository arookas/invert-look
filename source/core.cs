﻿
using BepInEx;
using BepInEx.Logging;
using HarmonyLib;
using UnityEngine;

namespace arookas.InvertLook {

/// <summary>
/// Base plugin class.
/// </summary>
[BepInDependency(
  PluginGuid.CONTROL_COMPANY,
  BepInDependency.DependencyFlags.SoftDependency
)]
[BepInDependency(
  PluginGuid.IMPERIUM,
  BepInDependency.DependencyFlags.SoftDependency
)]
[BepInDependency(
  PluginGuid.LETHAL_CONFIG,
  BepInDependency.DependencyFlags.SoftDependency
)]
[BepInDependency(
  PluginGuid.TOO_MANY_EMOTES,
  BepInDependency.DependencyFlags.SoftDependency
)]
[BepInPlugin(PLUGIN_GUID, PLUGIN_NAME, PLUGIN_VERSION)]
public class Plugin : BaseUnityPlugin {

  public const string PLUGIN_GUID    = "arookas.InvertLook";
  public const string PLUGIN_NAME    = "Invert Look";
  public const string PLUGIN_VERSION = "0.2.0";

  /// <summary>
  /// Access the global instance of the plugin, once initiated by BepInEx.
  /// </summary>
  public static Plugin Instance { get; private set; }

  /// <summary>
  /// Access the ManualLogSource to the global instance of the plugin.
  /// </summary>
  public static ManualLogSource LogSource => Instance.Logger;

  /// <summary>
  /// Access the Harmony instance of the plugin.
  /// </summary>
  protected Harmony Harmony { get; set; }

  /// <summary>
  /// Access the BepInEx config settings.
  /// </summary>
  public PluginSettings Settings { get; private set; }

  /// <summary>
  /// Initializes the plugin and Harmony patches.
  /// </summary>
  public void Awake() {
    if (Instance == null) {
      Instance = this;
    }

    LogSource.LogInfo($"Invert Look v{PLUGIN_VERSION} loaded");

    Settings = new PluginSettings(Config);
    Harmony  = new Harmony(PLUGIN_GUID);

    Harmony.PatchAll(typeof(LethalCompanyPatch));

    if (ControlCompanyPatch.Enabled) {
      Harmony.PatchAll(typeof(ControlCompanyPatch));
    }

    if (ImperiumPatch.Enabled) {
      Harmony.PatchAll(typeof(ImperiumPatch));
    }

    if (TooManyEmotesPatch.Enabled) {
      Harmony.PatchAll(typeof(TooManyEmotesPatch));
    }
  }

  /// <summary>
  /// Checks if a given BepInEx plugin is loaded. Shortcut for
  /// <c>BepInEx.Bootstrap.Chainloader.PluginInfos.ContainsKey</c>.
  /// </summary>
  /// <param name="key">
  /// Plugin GUID to check.
  /// </param>
  /// <returns>
  /// Whether the given BepInEx plugin is loaded.
  /// </returns>
  public static bool IsPluginLoaded(string key) {
    return BepInEx.Bootstrap.Chainloader.PluginInfos.ContainsKey(key);
  }

  /// <summary>
  /// Performs invert logic on a 2D input vector.
  /// Uses the plugin's config entries for each axis.
  /// </summary>
  /// <param name="vector">
  /// 2D input look vector.
  /// </param>
  /// <returns>
  /// Look vector, possibly inverted on its axes based on user settings.
  /// </returns>
  public static Vector2 InvertLook(Vector2 vector) {
    if (Instance.Settings.InvertX.Value) {
      vector.x *= -1.0F;
    }

    if (Instance.Settings.InvertY.Value) {
      vector.y *= -1.0F;
    }

    return vector;
  }

}

}